package fr.ulille.iut.tva.dto;

public class InfoTauxDTO {
	private String label;
    private double taux;
    
    public InfoTauxDTO() {}

    public InfoTauxDTO(String label, double taux) {
        this.label = label;
        this.taux = taux;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public double getTaux() {
        return taux;
    }

    public void setTaux(double taux) {
        this.taux = taux;
    }

}
