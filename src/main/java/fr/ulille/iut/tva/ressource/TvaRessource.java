package fr.ulille.iut.tva.ressource;

import java.util.ArrayList;
import java.util.List;

import fr.ulille.iut.tva.dto.InfoTauxDTO;
import fr.ulille.iut.tva.dto.detailsDTO;
import fr.ulille.iut.tva.service.CalculTva;
import fr.ulille.iut.tva.service.TauxTva;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

/**
 * TvaRessource
 */
@Path("tva")
public class TvaRessource {
    private CalculTva calculTva = new CalculTva();
    
    @GET
    @Path("tauxpardefaut")
    public double getValeurTauxParDefaut() {
    	return TauxTva.NORMAL.taux;
    }
    
    @GET
    @Path("valeur/{niveauTva}")
    public double getValeurTaux(@PathParam("niveauTva") String niveau) {
        try {
            return TauxTva.valueOf(niveau.toUpperCase()).taux; 
        }
        catch ( Exception ex ) {
            throw new NiveauTvaInexistantException();
        }
    }
    
    @GET
    @Path("valeur/{niveauTva}")
    public double getMontantTotal(@PathParam("niveauTva") String niveau, @QueryParam("somme") double somme) {
    	try {
    		return somme * ((TauxTva.valueOf(niveau.toUpperCase()).taux / 100)+1);
    	}
    	catch(Exception ex) {
    		throw new NiveauTvaInexistantException();
    	}
    }
    @GET
    @Path("lestaux")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<InfoTauxDTO> getInfoTaux() {
      ArrayList<InfoTauxDTO> result = new ArrayList<InfoTauxDTO>();
      for ( TauxTva t : TauxTva.values() ) {
        result.add(new InfoTauxDTO(t.name(), t.taux));
      }
      return result;
    }

    @GET
    @Path("details/{niveauTva}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<detailsDTO> getDetail(@PathParam("niveauTva") String niveau, @QueryParam("somme") double somme){
    	ArrayList<detailsDTO> result = new ArrayList<detailsDTO>();
    	result.add(new detailsDTO(getMontantTotal(niveau, somme), TauxTva.valueOf(niveau.toUpperCase()).taux, somme, niveau, getValeurTaux(niveau)));
    	return result;
    }

}
